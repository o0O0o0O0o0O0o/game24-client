package clientSrc.paint;

/**
 * @author dingsy
 * @date 2022/12/1
 * @apiNote
 */
public interface GameEvent {

    boolean excute();

}
