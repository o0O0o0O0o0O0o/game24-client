package clientSrc.paint;

import clientSrc.effect.BigSignSpecialEffect;
import clientSrc.effect.BigWordSpecialEffect;
import clientSrc.effect.ResultTableEffect;
import clientSrc.effect.SpecialEffectManager;
import clientSrc.entity.Card;
import clientSrc.entity.SynAllDataVo;
import clientSrc.utils.G2dConfig;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * @author dingsy
 * @date 2022/12/12
 * @apiNote
 */
public class TestMain {

    static int frameWidth = 800;
    static int frameHeight = 500;

//    public static void main(String[] args) {
//        Game game = new Game();
//        game.runGame();
//        testData(game);
//        testEffect(game);
////        testEffectTable(game);
//
//    }

    public static void testEffect(Game game) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SpecialEffectManager manager = game.gamePainter.getEffectManager();

//        manager.addEffect(new BigSignSpecialEffect(G2dConfig.vieImg, 200, 200, 400, 400));
//        manager.addEffect(new BigWordSpecialEffect("3", 200, 200));

        int x1 = 260;
        int y1 = 240;

        BigSignSpecialEffect signSpecialEffect = new BigSignSpecialEffect(G2dConfig.vieImg, x1, y1);
        game.gamePainter.getEffectManager().addEffect(signSpecialEffect);
        // 大字特效
        game.gamePainter.getEffectManager().addEffect(new BigWordSpecialEffect("PPP" + "：" + "(6-2-1)x8", x1-90, y1+70, game.gamePainter));


    }

    private static void testEffectTable(Game game) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SpecialEffectManager manager = game.gamePainter.getEffectManager();

        Object[][] results = new Object[3][2];
        results[0] = new String[]{"昵称", "分数"};
        results[1] = new String[]{"3", "5"};
        results[2] = new Integer[]{5, 9};
        // 创建展示表格
        JTable resultTable;
        resultTable = new JTable(results, results[0]);
        resultTable.setSize(300, 300);
        resultTable.setLocation(200,200);
        resultTable.setShowGrid (false);
        resultTable.setBackground(Color.gray);
        resultTable.setFont(G2dConfig.table_title_font);
        resultTable.setRowHeight(30);
        resultTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        resultTable.setVisible(true);


        game.gamePainter.add(resultTable);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < results[0].length; i++) {
            resultTable.getColumnModel().getColumn(i).setMaxWidth(300/results[0].length);
            resultTable.getColumnModel().getColumn(i).setMinWidth(300/results[0].length);
        }
        manager.addEffect(new ResultTableEffect(300, 300, resultTable));

    }

    public static void testData(Game game) {
        SynAllDataVo dataVo = new SynAllDataVo();
        game.gamePainter.setSynAllDataVo(dataVo);
        dataVo.setCardHeapNum(67);
        dataVo.setVieCard(new Card(2));

        // 我
        SynAllDataVo.PlayerVo me = new SynAllDataVo.PlayerVo();
        dataVo.setMe(me);
        me.setStatus(3);
        me.setName("蔡徐坤");
        me.setHandCardNum(3); // 手牌数量
        me.setBackCardNum(2); // 备用牌数量
        me.setBackCard(new ArrayList<>());
        me.setHandCard(new ArrayList<>());
        me.getBackCard().add(new Card(1));
        me.getBackCard().add(new Card(2));
        me.getHandCard().add(new Card(3));
        me.getHandCard().add(new Card(4));
        me.getHandCard().add(new Card(5));
        me.setBackCardSize(2);
        me.setHandCardSize(3);
        me.setTempCard(new Card(9));

        dataVo.setDeskPlayerList(new ArrayList<>());
        SynAllDataVo.PlayerVo p1 = new SynAllDataVo.PlayerVo();
        dataVo.getDeskPlayerList().add(p1);
        p1.setName("玩家1");
        p1.setScore(34);
        p1.setBackCardNum(2);
        p1.setHandCardNum(3);
        p1.setBackCardSize(2);
        p1.setHandCardSize(3);
        p1.setTempCard(new Card(9));

        SynAllDataVo.PlayerVo p2 = new SynAllDataVo.PlayerVo();
        dataVo.getDeskPlayerList().add(p2);
        p2.setName("玩家2");
        p2.setScore(347.3);
        p2.setBackCardNum(0);
        p2.setHandCardNum(2);
        p2.setBackCardSize(0);
        p2.setHandCardSize(2);
    }
}
