package clientSrc.paint;

import clientSrc.effect.SpecialEffectManager;
import clientSrc.entity.Card;
import clientSrc.entity.SynAllDataVo;
import clientSrc.utils.G2dConfig;
import lombok.Data;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author dingsy
 * @date 2022/12/16
 * @apiNote
 */
@Data
public class GamePainter extends JPanel{

    int frameWidth = 800;
    int frameHeight = 500;

    private Game game;
    private JFrame mainFrame;
//    private SpringLayout layout;
    private CardSiteCalculator siteCalculator = new CardSiteCalculator();
    private SpecialEffectManager effectManager = new SpecialEffectManager();
    @Setter
    private GameEventListener eventListener;

    private int cadreWidth = 65;
    private int cadreHeight = 100;

    // ================以上是不变参数 = 以下是变化参数===============
    @Setter
    private SynAllDataVo synAllDataVo;
    JTextField textField;
    JButton vieBtn;
    JButton readyBtn;
    JButton giveUpBtn;
    // 牌堆位置
    private int[] cardHeapSite = new int[4];
    boolean waiting;


    public GamePainter(Game game) {
        this.game = game;
    }

    void initNotOk() {
        runFrame();
//        initTextField();
//        initVieBtn();
//        initReadyBtn();
//        initGiveUpBtn();
    }
    void initOk() {
        game.requestSender.syn();
    }

    public void runFrame() {
        mainFrame = new JFrame("24点");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(frameWidth, frameHeight);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)screenSize.getHeight();
        int width = (int)screenSize.getWidth();
        mainFrame.setLocation((width - frameWidth)/2, (height - frameHeight)/2);
        mainFrame.setVisible(true);
        this.isDoubleBuffered();
        this.setLayout(null);
        mainFrame.add(this);
        frameWidth = mainFrame.getWidth();
        frameHeight = mainFrame.getHeight()-30;
    }

    private void initTextField() {

        textField = new JTextField("");
        textField.setSize(300, 30);
        textField.setVisible(true);
        textField.setFont(G2dConfig.text_field_font);
        textField.setLocation(80, frameHeight - 45);
        this.add(textField);
        textField.addActionListener(e -> {
            String actionCommand = e.getActionCommand();
//            System.out.println(actionCommand);
        });

//        layout.putConstraint(SpringLayout.WEST, textField, 80, SpringLayout.WEST, this);
//        layout.putConstraint(SpringLayout.EAST, textField, 380, SpringLayout.WEST, this);
//        layout.putConstraint(SpringLayout.SOUTH, textField, -15, SpringLayout.SOUTH, this);
//        layout.putConstraint(SpringLayout.NORTH, textField, -45, SpringLayout.SOUTH, this);
    }

    private void initVieBtn() {
        vieBtn = new JButton("抢");
        vieBtn.setSize(60, 30);
        vieBtn.setVisible(true);
        vieBtn.setFont(G2dConfig.text_field_font);
        vieBtn.setLocation(380, frameHeight - 45);
        this.add(vieBtn);
        vieBtn.addActionListener(e -> {
            String actionCommand = e.getActionCommand();
//            System.out.println("抢" + actionCommand);
            game.getRequestSender().vie(textField.getText());
        });

//        layout.putConstraint(SpringLayout.WEST, vieBtn, 380, SpringLayout.WEST, this);
//        layout.putConstraint(SpringLayout.EAST, vieBtn, 380, SpringLayout.WEST, this);
//        layout.putConstraint(SpringLayout.SOUTH, vieBtn, -15, SpringLayout.SOUTH, this);
//        layout.putConstraint(SpringLayout.NORTH, vieBtn, -45, SpringLayout.SOUTH, this);
    }

    private void checkVieBtnStatus() {
        if(synAllDataVo.getMe().getStatus() == 5) {
            vieBtn.setEnabled(true);
            vieBtn.setBackground(G2dConfig.orange);
        }else {
            vieBtn.setEnabled(false);
            vieBtn.setBackground(G2dConfig.dark_orange);
        }
    }

    private void initGiveUpBtn() {
        giveUpBtn = new JButton("放弃");
        giveUpBtn.setSize(70, 30);
        giveUpBtn.setVisible(true);
        giveUpBtn.setFont(G2dConfig.text_field_font);
        giveUpBtn.setLocation(450, frameHeight - 45);
        this.add(giveUpBtn);
        giveUpBtn.addActionListener(e -> {
            String actionCommand = e.getActionCommand();
//            System.out.println("抢" + actionCommand);
            game.getRequestSender().giveUp();
        });

//        layout.putConstraint(SpringLayout.WEST, giveUpBtn, 450, SpringLayout.WEST, this);
//        layout.putConstraint(SpringLayout.EAST, vieBtn, 380, SpringLayout.WEST, this);
//        layout.putConstraint(SpringLayout.SOUTH, giveUpBtn, -15, SpringLayout.SOUTH, this);
//        layout.putConstraint(SpringLayout.NORTH, vieBtn, -45, SpringLayout.SOUTH, this);
    }

    private void checkGiveUpBtnStatus() {
        if(synAllDataVo.getMe().getStatus() == 5) {
            giveUpBtn.setEnabled(true);
            giveUpBtn.setBackground(G2dConfig.orange);
        }else {
            giveUpBtn.setEnabled(false);
            giveUpBtn.setBackground(G2dConfig.dark_orange);
        }
    }

    private void initReadyBtn() {
        readyBtn = new JButton("准备");
        readyBtn.setSize(70, 30);
        readyBtn.setVisible(true);
        readyBtn.setFont(G2dConfig.text_field_font);
        readyBtn.setLocation(120, frameHeight/2 + 10);
        readyBtn.setBackground(Color.orange);
        this.add(readyBtn);
        readyBtn.addActionListener(e -> {
            String actionCommand = e.getActionCommand();
//            System.out.println(actionCommand);
            game.getRequestSender().ready();
        });

//        layout.putConstraint(SpringLayout.WEST, readyBtn, 120, SpringLayout.WEST, this);
    }

    private void checkReadyBtnStatus() {
        int status = synAllDataVo.getMe().getStatus();
        if(status == 3 || status == 1) {
            readyBtn.setVisible(true);
            readyBtn.setEnabled(true);
            readyBtn.setBackground(G2dConfig.orange);
        }else if(status == 4 || status == 2) {
            readyBtn.setVisible(true);
            readyBtn.setEnabled(false);
            readyBtn.setBackground(G2dConfig.dark_orange);
        }else {
            readyBtn.setVisible(false);
        }
    }

    @Override
    public void paint(Graphics g) {
        frameWidth = mainFrame.getWidth();
        frameHeight = mainFrame.getHeight()-30;
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        if(waiting) {
            paintWaiting(g2d);
            return;
        }
        if(synAllDataVo != null) {
            // 画分界线
            drawBorderLine(g2d);
            // 画公共信息
            drawPublicMsg(g2d);
            // 我方信息
            drawMy(g2d);
            // 画出对方信息
            drawYourMsg(g2d);
            // 如果没有抢牌 画空白牌
            drawBlankCard(g2d);
            // 控件
            checkComponet();
            // 重画子组件
            super.paintChildren(g);
            // 特效
            effectManager.paint(g2d);
        }
    }

    private void paintWaiting(Graphics2D g) {
        g.setFont(G2dConfig.table_title_font);
        g.drawString("正在连接服务器. . .", frameWidth/2 - 60, frameHeight/2);
        paintChildren(g);
    }

    // 画玩家状态的描述
    private void drawStatusStr(int status, Graphics2D g2d, float x, float y) {
        if(status == 2) {
            g2d.setColor(G2dConfig.green);
            g2d.drawString("已准备", x, y);
        }else if(status == 4) {
            g2d.setColor(G2dConfig.green);
            g2d.drawString("已准备", x, y);
        }else if(status == 6) {
            g2d.setColor(G2dConfig.red);
            g2d.drawString("放弃", x, y);
        }
    }

    private void drawBlankCard(Graphics2D g2d) {
        Image image = null;
        if(synAllDataVo.getVieCard() == null) {
            image = G2dConfig.blankImg;
        }else {
            Card vieCard = synAllDataVo.getVieCard();
            image = G2dConfig.getCardImg(vieCard.getNum() + "");
        }
        // 画抢牌区
        g2d.drawImage(image, vieCardX1, vieCardY1, cadreWidth, cadreHeight, null);
    }

    // 竖分界线宽度
    double longWidth = frameWidth * 0.8;
    private int vieCardX1;
    private int vieCardY1;
    // 画分界线
    private void drawBorderLine(Graphics2D g2d) {
        longWidth = frameWidth * 0.8;
        g2d.setColor(G2dConfig.darkGray);
        g2d.setStroke(G2dConfig.粗实线);
        // 竖线
        g2d.drawLine((int) longWidth-1, 0, (int) longWidth-1, frameHeight);
        g2d.drawLine((int) longWidth+1, 0, (int) longWidth+1, frameHeight);
        // 横线
        g2d.drawLine(0, frameHeight/2 -1, (int) longWidth-1, frameHeight/2 -1);
        g2d.drawLine(0, frameHeight/2 +1, (int) longWidth-1, frameHeight/2 +1);
        // 抢牌框
        double x1 = (longWidth - cadreWidth) / 2;
        double y1 = (frameHeight - cadreHeight) / 2;
        vieCardX1 = (int)x1+1; vieCardY1 = (int)y1+1;
        g2d.drawRect((int) x1, (int) y1, cadreWidth+2, cadreHeight+2);
    }

    // 我方信息
    private void drawMy(Graphics2D g2d) {
        SynAllDataVo.PlayerVo me = synAllDataVo.getMe();
        // 名字 分数 状态
        g2d.setFont(G2dConfig.name_score_font);
        g2d.drawString(synAllDataVo.getMe().getName(), 15, frameHeight/2 + 17);
        g2d.drawString("分数: " + me.getScore(), 15, frameHeight/2 + 37);
        drawStatusStr(me.getStatus(), g2d,  15, frameHeight/2 + 57);

        int inter = 20; // 牌间隔
        int bigInter = 45;  // 两类牌间隔
        if(me == null) return;
        int bn = me.getBackCardNum();
        int hn = me.getHandCardNum();
        double x1 = (longWidth - (bn + hn) * cadreWidth - (bn-1)*inter - (hn-1)*inter - bigInter)/2;
        double y1 = frameHeight/2 + 0.3 * frameHeight/2;
        // 备用牌坑位
        g2d.setColor(G2dConfig.black);
        g2d.setStroke(G2dConfig.粗虚线);
        if(bn > 0) {
            g2d.drawRect((int) x1-1, (int)y1-2, (int)(bn *cadreWidth + inter*(bn - 1))+3,(int)(cadreHeight)+3);
        }
        List<List<Integer>> backCardSite = new ArrayList<>();
        for(int i = 0; i < bn; i++) {
            backCardSite.add(Arrays.asList(new Integer[]{
                    (int) x1 + (cadreWidth + inter) * i,
                    (int) y1,
                    (int) x1 + (cadreWidth + inter) * i + cadreWidth,
                    (int) y1 + cadreHeight}));
        }

        // 出站牌坑位
        g2d.setStroke(new BasicStroke(3));
        int hx1 = (int) x1 + (bn == 0 ? 0 : (bn * (cadreWidth + inter) - inter + bigInter));
        if(hn > 0) {
            g2d.drawRect(hx1-1, (int)y1-2, (int)(hn *cadreWidth + inter*(hn - 1))+4,(int)(cadreHeight)+3);
        }
        List<List<Integer>> handCardSite = new ArrayList<>();
        for(int i = 0; i < hn; i++) {
            handCardSite.add(Arrays.asList(new Integer[]{
                    (int) hx1 + (cadreWidth + inter)*i,
                    (int) y1,
                    (int) hx1 + (cadreWidth + inter)*i + cadreWidth,
                    (int) y1 + cadreHeight}));
        }
        // 记录坐标
        siteCalculator.addSiteData(me.getId(), backCardSite, handCardSite);
/*        // 画临时牌
        if(me.getTempCard() != null) {
//            g2d.drawRect((int) x1-1-cadreWidth-inter, (int)y1-2, (int)(cadreWidth)+3,(int)(cadreHeight)+3);
            g2d.drawImage(G2dConfig.getCardImg(me.getTempCard().getNum() + ""), (int) x1 - cadreWidth - inter, (int) y1, cadreWidth, cadreHeight, null);
        }

        // 画牌
        for(int i = 0; i< me.getBackCardSize(); i++) {
            Card card = me.getBackCard().get(i);
            g2d.drawImage(G2dConfig.getCardImg(card.getNum() + ""), (int) x1 + (cadreWidth + inter)*i, (int) y1, cadreWidth, cadreHeight, null);
        }
        for(int i = 0; i< me.getHandCardSize(); i++) {
            Card card = me.getHandCard().get(i);
            g2d.drawImage(G2dConfig.getCardImg(card.getNum() + ""), (int) hx1 + (cadreWidth + inter)*i, (int) y1, cadreWidth, cadreHeight, null);
        }*/
        // 画牌
        cardPanelList.clear();
        String key;
        key = "0" + "0";
        if(me.getTempCard() != null) {
            if(!cardPanelMap.containsKey(key)) {
                CardPanel cardPanel = new CardPanel(game, me.getTempCard().getNum(), 0, (int) x1 - cadreWidth - inter, (int) y1, cadreWidth, cadreHeight);
                cardPanelMap.put(key, cardPanel);
                this.add(cardPanel);
            }
            CardPanel cardPanel = cardPanelMap.get(key);
            cardPanel.update(me.getTempCard().getNum(), 0, 0, (int) x1 - cadreWidth - inter, (int) y1, cadreWidth, cadreHeight);
            cardPanelList.add(cardPanel);
        }
        Card card;
        for(int i = 0; i< me.getBackCardSize(); i++) {
            card = me.getBackCard().get(i);
            key = "1" + i;
            if(!cardPanelMap.containsKey(key)) {
                CardPanel cardPanel = new CardPanel(game, card.getNum(), 1, (int) x1 + (cadreWidth + inter)*i, (int) y1, cadreWidth, cadreHeight);
                cardPanelMap.put(key, cardPanel);
                this.add(cardPanel);
            }
            CardPanel cardPanel = cardPanelMap.get(key);
            cardPanel.update(card.getNum(), 1, i, (int) x1 + (cadreWidth + inter)*i, (int) y1, cadreWidth, cadreHeight);
            cardPanelList.add(cardPanel);
        }
        for(int i = 0; i< me.getHandCardSize(); i++) {
            card = me.getHandCard().get(i);
            key = "2" + i;
            if(!cardPanelMap.containsKey(key)) {
                CardPanel cardPanel = new CardPanel(game, card.getNum(), 2, (int) hx1 + (cadreWidth + inter)*i, (int) y1, cadreWidth, cadreHeight);
                cardPanelMap.put(key, cardPanel);
                this.add(cardPanel);
            }
            CardPanel cardPanel = cardPanelMap.get(key);
            cardPanel.update(card.getNum(), 2, i, (int) hx1 + (cadreWidth + inter)*i, (int) y1, cadreWidth, cadreHeight);
            cardPanelList.add(cardPanel);
        }
        for(CardPanel cardPanel: cardPanelMap.values()) {
            if(!cardPanelList.contains(cardPanel)) {
                cardPanel.setVisible(false);
            }else {
                cardPanel.setVisible(true);
            }
        }
        // 高亮
        g2d.setColor(G2dConfig.yellow2);
        g2d.setStroke(G2dConfig.粗虚线);
        if(tar != null) {
            g2d.drawRect(tar.getX1()-2, tar.getY1()-2, tar.getCardWidth()+4, tar.getCardHeight()+3);
        }else {
            if(res != null) {
                g2d.drawRect(res.getX1()-2, res.getY1()-2, res.getCardWidth()+4, res.getCardHeight()+3);
            }
        }

    }


    // 画公共信息
    private void drawPublicMsg(Graphics2D g2d) {
        int x1 = (int)((frameWidth-longWidth-cadreWidth)/2 + longWidth) - 8;
        int y1 = (int)((frameHeight-cadreHeight)/2);
        int cardHeapNum = synAllDataVo.getCardHeapNum();
        // 牌堆
        g2d.setColor(G2dConfig.darkGray);
        g2d.setStroke(G2dConfig.普通细线);
        if(cardHeapNum == 0) {
            g2d.drawRect(x1+6, y1+8, cadreWidth, cadreHeight);
        }
        if(cardHeapNum >= 1) {
            g2d.drawImage(G2dConfig.cardBackImg, x1+6, y1+8, cadreWidth, cadreHeight, null);
        }
        if(cardHeapNum >= 2) {
            g2d.drawImage(G2dConfig.cardBackImg, x1+3, y1+4, cadreWidth, cadreHeight, null);
        }
        if(cardHeapNum >= 3) {
            g2d.drawImage(G2dConfig.cardBackImg, x1, y1, cadreWidth, cadreHeight, null);
        }
        g2d.drawString("剩余 " + cardHeapNum + " 张", x1, y1 - 5);
        cardHeapSite[0] = x1; cardHeapSite[1] = y1; cardHeapSite[2] = x1 + cadreHeight; cardHeapSite[3] = y1 + cadreHeight;
    }

    // 画出对方所有人信息
    private void drawYourMsg(Graphics2D g2d) {
        List<SynAllDataVo.PlayerVo> deskPlayerList = synAllDataVo.getDeskPlayerList();
        int yourSize = deskPlayerList.size();
        double itemWidth = longWidth / yourSize;
        g2d.setColor(G2dConfig.darkGray);
        g2d.setStroke(G2dConfig.粗实线);
        // 画分界线
        for(int i = 1; i < yourSize; i++) {
            g2d.drawLine((int) (itemWidth * i), 0, (int) (itemWidth * i), frameHeight/2);
        }
        g2d.setFont(G2dConfig.name_score_font);
        for(int i = 0; i < deskPlayerList.size(); i++) {
            SynAllDataVo.PlayerVo playerVo = deskPlayerList.get(i);
            // 名字 分数 状态
            g2d.drawString(playerVo.getName(), (float) (itemWidth * i + 15), 17);
            g2d.drawString("分数：" + playerVo.getScore()+"", (float) (itemWidth * i + 15), 37);
            drawStatusStr(playerVo.getStatus(), g2d, (float) (itemWidth * i + 15), 57);
            // 计算牌位置
            List<List<Integer>>[] siteData = siteCalculator.calculateAndUpdate(playerVo.getId(), (int) (itemWidth * i), 0, (int) (itemWidth * (i + 1)), frameHeight / 2, cadreWidth, cadreHeight, playerVo.getBackCardNum(), playerVo.getHandCardNum());
            // 画备用牌背面
            for(int j = 0; j < playerVo.getBackCardSize(); j++) {
                List<Integer> point = siteData[0].get(j);
                g2d.drawImage(G2dConfig.cardBackImg, point.get(0), point.get(1), point.get(2) - point.get(0), point.get(3) - point.get(1), null);
            }
            // 画手牌背面
            for(int j = 0; j < playerVo.getHandCardSize(); j++) {
                List<Integer> point = siteData[1].get(j);
                g2d.drawImage(G2dConfig.cardBackImg, point.get(0), point.get(1), point.get(2) - point.get(0), point.get(3) - point.get(1), null);
            }
        }
    }

    // 控件
    private void checkComponet() {
        if(readyBtn == null) initReadyBtn();
        if(vieBtn == null) initVieBtn();
        if(giveUpBtn == null) initGiveUpBtn();
        if(textField == null) initTextField();

        readyBtn.setLocation(120, frameHeight/2 + 10);
        vieBtn.setLocation(380, frameHeight - 45);
        giveUpBtn.setLocation(450, frameHeight - 45);
        textField.setLocation(80, frameHeight - 45);
        textField.setSize(300, 30);
        checkVieBtnStatus();
        checkReadyBtnStatus();
        checkGiveUpBtnStatus();
    }

    private CardPanel res;
    private CardPanel tar;
    private java.util.List<CardPanel> cardPanelList = new ArrayList<>();
    private Map<String, CardPanel> cardPanelMap = new HashMap<>();

    // 牌拖动时
    public void moveing(CardPanel cardPanel) {
        res = cardPanel;
        int x0 = cardPanel.getX();
        int y0 = cardPanel.getY();
        int minLocate = 10000;
        CardPanel minTar = null;
        for(CardPanel cardPanelItem: cardPanelList) {
            int x = cardPanelItem.getX1();
            int y = cardPanelItem.getY1();
            double sqrt = Math.sqrt((x0 - x) * (x0 - x) + (y0 - y) * (y0 - y));
            if(sqrt < minLocate) {
                minLocate = (int) sqrt;
                minTar = cardPanelItem;
            }
        }
        if(tar != null) {
//            tar.setHighLightState(false);
            tar = null;
        }
        if(minLocate < 100) {
//            res.setHighLightState(false);
            tar = minTar;
//            tar.setHighLightState(true);
//            System.out.println(tar.getNum());
        }else {
//            res.setHighLightState(true);
        }
        repaint();

    }

    // 牌拖动释放时
    public void tryExchange() {
        if(res != null) {
            if(tar != null && res != tar) {
                // 换
                int tempX1 = res.getX1();
                int tempY1 = res.getY1();
                res.setX1(tar.getX1());
                res.setY1(tar.getY1());
                tar.setX1(tempX1);
                tar.setY1(tempY1);
                res.setLocation(res.getX1(), res.getY1());
                tar.setLocation(tar.getX1(), tar.getY1());

/*                int tempTYpe = res.getType();
                int tempIndex = res.getIndex();
                res.setType(tar.getType());
                res.setIndex(tar.getIndex());
                tar.setType(tempTYpe);
                tar.setIndex(tempIndex);
                cardPanelMap.put(tar.getType() + "" + tar.getIndex(), tar);
                cardPanelMap.put(res.getType() + "" + res.getIndex(), res);*/
                game.requestSender.exchange(res.getType(), res.getIndex(), tar.getType(), tar.getIndex());

            }else {
                // 不换 归位
                res.setLocation(res.getX1(), res.getY1());
            }
        }
        res = null; tar = null;
        repaint();
    }

}
