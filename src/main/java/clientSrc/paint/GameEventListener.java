package clientSrc.paint;

import java.util.LinkedList;

/**
 * @author dingsy
 * @date 2022/12/1
 * @apiNote
 */
public class GameEventListener implements Runnable{

    public Thread listenerThread;
    public boolean status = false;

    private LinkedList<GameEvent> eventList = new LinkedList<>();

    public void addEvent(GameEvent event) {
        eventList.offer(event);
    }

    public GameEvent nextEvent() {
        return eventList.removeFirst();
    }

    public int size() {
        return eventList.size();
    }

    public Thread startListen(int tempTimeMillion) {
        this.tempTimeMillion = tempTimeMillion;
        listenerThread = new Thread(this);
        status = true;
        listenerThread.start();
        return listenerThread;
    }

    public void stopListen() {
        if(status) {
            listenerThread.stop();
        }
    }

    int tempTimeMillion = 10;

    @Override
    public void run() {
        while(true) {
            int size = size();
            for(int i = 0; i < size; i++) {
                GameEvent event = nextEvent();
                if(event.excute()) {
                    addEvent(event);
                }
            }
            try {
                Thread.sleep(tempTimeMillion);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
