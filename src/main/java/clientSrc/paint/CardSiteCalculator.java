package clientSrc.paint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 牌的位置的计算器
public class CardSiteCalculator {

    private Map<String, List<List<Integer>>> backData = new HashMap<>();
    private Map<String, List<List<Integer>>> handData = new HashMap<>();

    // 查询谁的第几章备用牌的位置
    public List<Integer> searchBackCardSite(String playerId, int num) {
        return backData.get(playerId).get(num);
    }

    // 查询谁的第几章手牌的位置
    public List<Integer> searchHandCardSite(String playerId, int num) {
        return handData.get(playerId).get(num);
    }

    public void removeSite(String playerId) {
        backData.remove(playerId);
        handData.remove(playerId);
    }

    public void addSiteData(String playerId, List<List<Integer>> backDataSite, List<List<Integer>> handDataSite) {
        backData.put(playerId, backDataSite);
        handData.put(playerId, handDataSite);
    }

    public List<List<Integer>>[] calculateAndUpdate(String playerId, int outX1, int outY1, int outX2, int outY2, int cardWidth, int cardHeight, int bn, int hn) {
        List<List<Integer>>[] lists = calculate(outX1, outY1, outX2, outY2, cardWidth, cardHeight, bn, hn);
        backData.put(playerId, lists[0]);
        handData.put(playerId, lists[1]);
        return lists;
    }


        /**
         * [
         *  [x1,y1,x2,y2,0],
         *  [x1,y1,x2,y2,1]
         *  ...
         * ]
         * @param outX1
         * @param outY1
         * @param outX2
         * @param outY2
         * @param cardWidth
         * @param cardHeight
         * @param bn
         * @param hn
         * @return
         */
    public List<List<Integer>>[] calculate(int outX1, int outY1, int outX2, int outY2, int cardWidth, int cardHeight, int bn, int hn) {
        int inter = 25; // 牌间隔
        int bigInter = 40;  // 两类牌间隔
        double yPercent = 0.3;

        List<List<Integer>> backSite = new ArrayList<>();
        List<List<Integer>> handSite = new ArrayList<>();

        double y1 = outY1 + yPercent * (outY2-outY1);
        // 布局1
        int totalWidth = (bn + hn) * cardWidth + (bn == 0 ? 0 : (bn-1)*inter + bigInter) + (hn-1)*inter;
        if(totalWidth + 40 <= outX2 - outX1) {
            double x1 = (outX2 - outX1 - totalWidth)/2 + outX1;
            for(int i = 0; i < bn; i++) {
                List<Integer> item = new ArrayList<>();
                backSite.add(item);
                item.add((int) (x1 + i*(cardWidth + inter)));    // x1
                item.add((int) y1); // y1
                item.add(item.get(0) + cardWidth);    // x2
                item.add(item.get(1) + cardHeight); // y2
            }
            if(bn != 0) {
                x1 += bn * cardWidth + (bn-1) * inter + bigInter;
            }
            for(int i = 0; i < hn; i++) {
                List<Integer> item = new ArrayList<>();
                handSite.add(item);
                item.add((int) ((x1 + i*(cardWidth + inter))));
                item.add((int) y1);
                item.add(item.get(0) + cardWidth);    // x2
                item.add(item.get(1) + cardHeight); // y2
            }
        // 布局2
        }else if((bn + hn) * cardWidth + inter <= outX2 - outX1) {
            totalWidth = (bn + hn) * cardWidth + inter;
            double x1 = (outX2 - outX1 - totalWidth)/2 + outX1;
            for(int i = 0; i < bn; i++) {
                List<Integer> item = new ArrayList<>();
                backSite.add(item);
                item.add((int) (x1 + i*(cardWidth)));    // x1
                item.add((int) y1); // y1
                item.add(item.get(0) + cardWidth);    // x2
                item.add(item.get(1) + cardHeight); // y2
            }
            if(bn != 0) {
                x1 += bn * cardWidth + inter;
            }
            for(int i = 0; i < hn; i++) {
                List<Integer> item = new ArrayList<>();
                handSite.add(item);
                item.add((int) ((x1 + i*cardWidth)));
                item.add((int) y1);
                item.add(item.get(0) + cardWidth);    // x2
                item.add(item.get(1) + cardHeight); // y2
            }
        // 布局3
        }else {
            totalWidth = (bn + hn) * cardWidth/2 + inter/2;
            double x1 = (outX2 - outX1 - totalWidth)/2 + outX1;
            for(int i = 0; i < bn; i++) {
                List<Integer> item = new ArrayList<>();
                backSite.add(item);
                item.add((int) (x1 + i*(cardWidth/2)));    // x1
                item.add((int) y1); // y1
                item.add(item.get(0) + cardWidth/2);    // x2
                item.add(item.get(1) + cardHeight/2); // y2
            }
            if(bn != 0) {
                x1 += bn * cardWidth/2 + inter/2;
            }
            for(int i = 0; i < hn; i++) {
                List<Integer> item = new ArrayList<>();
                handSite.add(item);
                item.add((int) ((x1 + i*cardWidth/2)));
                item.add((int) y1);
                item.add(item.get(0) + cardWidth/2);    // x2
                item.add(item.get(1) + cardHeight/2); // y2
            }
        }
        return new List[]{backSite, handSite};
    }

}
