package clientSrc.paint;

import clientSrc.RequestAccepter;
import clientSrc.RequestSender;
import clientSrc.effect.BigSignSpecialEffect;
import clientSrc.effect.BigWordSpecialEffect;
import clientSrc.effect.FlyCardSpecialEffect;
import clientSrc.effect.ResultTableEffect;
import clientSrc.entity.Card;
import clientSrc.entity.SynAllDataVo;
import clientSrc.utils.G2dConfig;
import clientSrc.utils.RefreshUtil;
import lombok.Getter;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author dingsy
 * @date 2022/12/12
 * @apiNote
 */
public class Game implements Runnable{

//    public static String host = "localhost";
    public static String host = "43.143.125.51";
    public static int port = 10024;


    public Socket socket;
    @Getter
    public RequestSender requestSender;
    public RequestAccepter requestAccepter;
    public Thread listenThread;
    public GameEventListener eventListener;
    @Getter
    public GamePainter gamePainter;
    public boolean initOk = false;
    private String name = "小赤佬";

    public void runGame() {

        requestAccepter = new RequestAccepter(this);
        eventListener = new GameEventListener();
        gamePainter = new GamePainter(this);
        gamePainter.setEventListener(eventListener);
        gamePainter.initNotOk();

        socket = runSocket();    // 连接服务器
        requestSender = new RequestSender(socket);
        listenThread = new Thread(this);
        listenThread.start();

        eventListener.addEvent(() -> {
            do {
                name = (String) JOptionPane.showInputDialog(null, "你的昵称：", "", JOptionPane.PLAIN_MESSAGE, new ImageIcon(), null, null);
            } while (name == null || name.isEmpty());
            // changeName
            requestSender.changeName(name);
            initOk = true;
            return false;
        });
        gamePainter.initOk();

        eventListener.startListen(20);

        eventListener.addEvent(new GameEvent() {
            long time = System.currentTimeMillis();
            @Override
            public boolean excute() {
                long curr = System.currentTimeMillis();
                if(curr - time > RefreshUtil.refreshTime) {
                    time = curr;
                    gamePainter.repaint();
                }
                return true;
            }
        });

    }


    public Socket runSocket() {
        long tryTime = 5000;
        long curr;
        gamePainter.waiting = true;
        gamePainter.repaint();
        while (true) {
            long time = System.currentTimeMillis();
            do {
                try {
                    Socket socket = new Socket(host, port);
//                    System.out.println("连接上服务器");
                    gamePainter.waiting = false;
                    return socket;
                } catch (IOException e) {

                }
                curr = System.currentTimeMillis();
            }while (socket == null && (curr - time <= tryTime));
            JOptionPane.showMessageDialog(null,"塞责 连不上服务器");
        }
    }

    @Override
    public void run() {
        if(socket != null) {
            try {
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);

                String line;
                while (true) {
                    line = reader.readLine();
                    if(line == null) {
                        continue;
                    }
                    if(requestAccepter != null) {
                        requestAccepter.processRequestLine(line);
                    }
                }
            } catch (IOException e) {
//                System.out.println("和服务器断开连接");
                JOptionPane.showMessageDialog(null,"塞责 和服务器断开连接了");
//                gamePainter.waiting = true;
//                runSocket();
//                requestSender = new RequestSender(socket);
//                listenThread = new Thread(this);
//                listenThread.start();
//                requestSender.changeName(name);
            }
        }
    }

    // 把牌堆的排发到用户手牌区域
    public void dealToHand(String playerId, Integer handIndex) {
        eventListener.addEvent(() -> {
            List<Integer> site = gamePainter.getSiteCalculator().searchHandCardSite(playerId, Integer.valueOf(handIndex));
            int[] cardHeapSite = gamePainter.getCardHeapSite();
            gamePainter.getEffectManager().addEffect(new FlyCardSpecialEffect(
                    G2dConfig.cardBackImg,
                    cardHeapSite[0], cardHeapSite[2], cardHeapSite[1], cardHeapSite[3],
                    site.get(0), site.get(2), site.get(1), site.get(3)));
            return false;
        });
    }

    // 通知客户端把牌堆的排发到抢牌区
    public void dealToVieCard(Card card) {
        eventListener.addEvent(() -> {
            int x1 = gamePainter.getVieCardX1();
            int y1 = gamePainter.getVieCardY1();
            int[] cardHeapSite = gamePainter.getCardHeapSite();
            gamePainter.getEffectManager().addEffect(new FlyCardSpecialEffect(
                    G2dConfig.cardBackImg,
                    cardHeapSite[0], cardHeapSite[2], cardHeapSite[1], cardHeapSite[3],
                    x1, x1 + gamePainter.getCadreWidth(), y1, y1 + gamePainter.getCadreHeight()));
            // 清空输入框
            gamePainter.getTextField().setText("");
            return false;
        });
    }

    // 通知客户端有用户抢牌
    public void vieCard(String playerId, String text) {
        Card vieCard = gamePainter.getSynAllDataVo().getVieCard();
        AtomicLong time = new AtomicLong();
        long t0 = System.currentTimeMillis();
        // 展示大图片："抢!"
        eventListener.addEvent(() -> {
            int x1 = gamePainter.getVieCardX1();
            int y1 = gamePainter.getVieCardY1();
            List<SynAllDataVo.PlayerVo> list = gamePainter.getSynAllDataVo().getDeskPlayerList();
            // 拿到抢牌者的名字
            String name = "";
            SynAllDataVo.PlayerVo me = gamePainter.getSynAllDataVo().getMe();
            if(me.getId().equals(playerId)) {
                name = me.getName();
            }else {
                for(SynAllDataVo.PlayerVo vo: list) {
                    if (vo.getId().equals(playerId)) {
                        name = vo.getName();
                        break;
                    }
                }
            }
            // 大图特效
            BigSignSpecialEffect signSpecialEffect = new BigSignSpecialEffect(G2dConfig.vieImg, x1 + 50, y1 + 40);
            time.set(signSpecialEffect.getStep2());
            gamePainter.getEffectManager().addEffect(signSpecialEffect);
            // 大字特效
            gamePainter.getEffectManager().addEffect(new BigWordSpecialEffect(name + "：" + text, x1 + 50, y1 + 103, gamePainter));
            return false;
        });
        // 抢牌区飞牌到抢拍人的 backcard 区域
        eventListener.addEvent(() -> {
            long curr = System.currentTimeMillis();
            if(curr - t0 >= time.get()) {
                List<Integer> site = gamePainter.getSiteCalculator().searchBackCardSite(playerId, 0);
                if(site == null || site.size() == 0) {
                    site = gamePainter.getSiteCalculator().searchHandCardSite(playerId, 0);
                }
                gamePainter.getEffectManager().addEffect(new FlyCardSpecialEffect(
                        G2dConfig.getCardImg(vieCard.getNum()+""),
                        gamePainter.getVieCardX1(),
                        gamePainter.getVieCardX1() + gamePainter.getCadreWidth(),
                        gamePainter.getVieCardY1(),
                        gamePainter.getVieCardY1() + gamePainter.getCadreHeight(),
                        site.get(0),
                        site.get(2),
                        site.get(1),
                        site.get(3)));
                return false;
            }
            return true;
        });
    }

    // 通知客户端展示提示框
    public void showTip(String text) {
        eventListener.addEvent(() -> {
            JOptionPane.showMessageDialog(null,text);
            return false;
        });
    }

    // 通知客户端展示大字
    public void showAllBigTitle(String title) {
        eventListener.addEvent(() -> {
            int x1 = gamePainter.getVieCardX1();
            int y1 = gamePainter.getVieCardY1();
            BigWordSpecialEffect effect = new BigWordSpecialEffect(title, x1 + 30, y1 + 50, gamePainter);
            effect.setStep2(1000);
            gamePainter.getEffectManager().addEffect(effect);
            return false;
        });
    }

    // 向客户端同步数据
    public void syn(SynAllDataVo dataVo) {
        if(initOk) {
            gamePainter.setSynAllDataVo(dataVo);
            gamePainter.repaint();
        }
    }

    private JTable resultTable;
    private int resultTableWidth = 250;
    private int resultTableHeight = 200;

    // 展示结果
    public void showResult(Object[][] results) {
        // 创建展示表格
        resultTable = new JTable(results, results[0]);
        gamePainter.add(resultTable);
        resultTable.setSize(resultTableWidth, results.length * 30 + 30);
        resultTable.setLocation(gamePainter.frameWidth * 1/3, gamePainter.frameHeight * 1/3);
        resultTable.setShowGrid (false);
        resultTable.setBackground(Color.gray);
        resultTable.setFont(G2dConfig.table_title_font);
        resultTable.setRowHeight(30);
        resultTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        for(int i = 0; i < results[0].length; i++) {
            resultTable.getColumnModel().getColumn(i).setMaxWidth(resultTableWidth/results[0].length);
            resultTable.getColumnModel().getColumn(i).setMinWidth(resultTableWidth/results[0].length);
        }
        gamePainter.getEffectManager().addEffect(new ResultTableEffect(resultTableWidth, resultTableHeight, resultTable));
    }

    public void updateImg(String text) {
        eventListener.addEvent(() -> {
            List<String> nameList = Arrays.asList(text.split("-"));
            G2dConfig.updateImg(nameList);
            return false;
        });
    }

}








