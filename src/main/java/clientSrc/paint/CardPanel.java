package clientSrc.paint;

import clientSrc.utils.G2dConfig;
import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

/**
 * @author dingsy
 * @date 2023/1/11
 * @apiNote
 */
@Data
public class CardPanel extends JPanel {

    private Integer num;
    private int type;  // 0 临时牌  1 备用牌  2 手牌
    private int cardWidth;
    private int cardHeight;
    private int x1;
    private int y1;
    private Game game;
    private JButton plusBtn;
    private JButton minusBtn;

    private int index;

    private boolean mouthInBtn = false;

    public void setNum(int num) {
        this.num = num;
    }
    public void setIndex(int index) {
        this.index = index;
    }

    public void updateXY(int x1, int y1) {
        if(this.x1 != x1 || this.y1 != y1) {
            this.x1 = x1;
            this.y1 = y1;
            this.setLocation(x1,y1);
        }
    }

    public void updateSize(int cardWidth, int cardHeight) {
        if(cardWidth != cardWidth || cardHeight != cardHeight) {
            this.cardWidth = cardWidth;
            this.cardHeight = cardHeight;
            this.setSize(cardWidth, cardHeight);
        }
    }

    public void update(Integer num, int type, int index, int x1, int y1, int cardWidth, int cardHeight) {
        this.num = num;
        this.type = type;
        this.index = index;
        updateXY(x1, y1);
        updateSize(cardWidth, cardHeight);
        this.setVisible(true);
    }

    public CardPanel(Game game, Integer num, int type, int x1, int y1, int cardWidth, int cardHeight) {
        super();
        this.setBackground(Color.gray);
        this.game = game;
        this.num = num;
        this.type = type;
        this.x1 = x1;
        this.y1 = y1;
        this.cardWidth = cardWidth;
        this.cardHeight = cardHeight;
        this.setLayout(null);
        init();
    }

    private int px;
    private int py;

    private void init() {
        this.setLocation(x1,y1);
        this.setSize(cardWidth,cardHeight);
        this.setVisible(true);
//            initPlusBtn();
//            initMinusBtn();
//            plusBtn.setVisible(true);
//            minusBtn.setVisible(true);

        CardPanel cardPanel = this;
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                Point po = e.getPoint();
                cardPanel.setLocation(po.x + cardPanel.getLocation().x - px, po.y + cardPanel.getLocation().y - py);
                game.gamePainter.moveing(cardPanel);
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                px = e.getX();
                py = e.getY();
                game.gamePainter.remove(cardPanel);
                game.gamePainter.add(cardPanel, 0);
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                game.gamePainter.tryExchange();
            }
        });
    }

    private void initPlusBtn() {
        plusBtn = new JButton();
        this.add(plusBtn);
        plusBtn.setVisible(false);
        plusBtn.setBackground(Color.GREEN);
        plusBtn.setSize(27, 27);
        plusBtn.setLocation(0, 0);
        plusBtn.setIcon(new Icon() {
            @Override
            public void paintIcon(Component c, Graphics g, int x, int y) {
                g.drawImage(G2dConfig.plusBtnImg, x, y, plusBtn.getWidth(), plusBtn.getHeight(), null);
            }
            @Override
            public int getIconWidth() {
                return plusBtn.getWidth();
            }
            @Override
            public int getIconHeight() {
                return plusBtn.getHeight();
            }
        });
        plusBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
//                System.out.println("++");
            }
        });

    }

    private void initMinusBtn() {
        minusBtn = new JButton();
        this.add(minusBtn);
        minusBtn.setVisible(false);
        minusBtn.setBackground(Color.GREEN);
        minusBtn.setSize(27, 27);
        minusBtn.setLocation(cardWidth - minusBtn.getWidth(), 0);
        minusBtn.setIcon(new Icon() {
            @Override
            public void paintIcon(Component c, Graphics g, int x, int y) {
                g.drawImage(G2dConfig.minusBtnImg, x, y, minusBtn.getWidth(), minusBtn.getHeight(), null);
            }
            @Override
            public int getIconWidth() {
                return minusBtn.getWidth();
            }
            @Override
            public int getIconHeight() {
                return minusBtn.getHeight();
            }
        });
        minusBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
//                System.out.println("--");
            }
        });

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if(num != null) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(G2dConfig.getCardImg(num+""), 0, 0, cardWidth, cardHeight, null);
        }
        super.paintChildren(g);
    }



}
