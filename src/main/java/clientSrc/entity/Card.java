package clientSrc.entity;

import lombok.Data;

/**
 * @author dingsy
 * @date 2022/11/29
 * @apiNote
 */
@Data
public class Card {

    private Integer num;

    private TYPE type;

    private String path;

    public Card(Integer num) {
        type = TYPE.NUM;
        this.num = num;
        path = "nofound.jpg";
    }
}
