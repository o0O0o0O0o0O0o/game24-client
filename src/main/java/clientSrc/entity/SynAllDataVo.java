package clientSrc.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dingsy
 * @date 2022/12/7
 * @apiNote
 */
@Data
public class SynAllDataVo implements Serializable {

    private int deskPlayerNum = 6;  // 桌上最大人数
    private int handCardNum = 3; // 手牌张数
    private int backCardNum = 1; // 备用牌张数
    private int giveCardIntervalMillisecond = 500; // 发牌间隔毫秒数

    private List<PlayerVo> deskPlayerList = new ArrayList<>();  // 除了自己以外的人
    private List<PlayerVo> audiencePlayerList = new ArrayList<>();
    private PlayerVo me;

    private int cardHeapNum;    // 牌堆剩下牌数
    private Card vieCard;   // 抢牌区


    @Data
    public static class PlayerVo implements Serializable{

        private String id;
        private String name;
        /**
         * 0 没名字
         * 1 可用默认状态
         * 2 已准备
         * 3 换牌
         * 4 换牌完毕 备抢
         * 5 可抢
         */
        private int status;
        private int handCardNum;
        private List<Card> handCard;
        private int backCardNum;
        private List<Card> backCard;
        private double score;
        private String calText;     // 算式
        private Card tempCard;  // 临时牌

        private int handCardSize;
        private int backCardSize;

    }

}
