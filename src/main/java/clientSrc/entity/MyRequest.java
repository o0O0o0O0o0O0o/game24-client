package clientSrc.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dingsy
 * @date 2022/11/30
 * @apiNote
 */
@Data
public class MyRequest {

    private String uri;

    private Map<String, Object> data = new HashMap<>();
}
