package clientSrc;

import clientSrc.entity.MyRequest;
import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author dingsy
 * @date 2022/12/8
 * @apiNote
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class RequestSender {

    private Socket socket;
    private PrintWriter writer;     // 不传就不会发请求

    public RequestSender(Socket socket) {
        this.socket = socket;
        if(socket != null) {
            try {
                writer = new PrintWriter(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
        OutputStream os = null;
        try {
            os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            writer = new PrintWriter(osw);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendRequest(MyRequest request) {
        if(writer != null) {
            String jsonString = JSON.toJSONString(request);
            writer.println(jsonString);
            writer.flush();
        }
    }

    /**
     * 改名字
     * api/game/changeName
     */
    public void changeName(String name) {
        MyRequest request = new MyRequest();
        request.setUri("api/game/changeName");
        request.getData().put("name", name);
        sendRequest(request);
    }

    /**
     * 某人准备
     * api/game/ready
     */
    public void ready() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/ready");
        sendRequest(request);
    }

    /**
     * 变成观众
     * api/game/toAudience
     */
    public void toAudience() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/toAudience");
        sendRequest(request);
    }

    /**
     * 变成桌上的人
     * api/game/toDeskPlayer
     */
    public void toDeskPlayer() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/toDeskPlayer");
        sendRequest(request);
    }

    /**
     * 退出房间
     * api/game/outRoom
     */
    public void outRoom() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/outRoom");
        sendRequest(request);
    }

    /**
     * 某人换牌完毕 准备
     * api/game/changeCardOver
     */
    public void changeCardOver() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/changeCardOver");
        sendRequest(request);
    }

    /**
     *  抢
     *  api/game/vie
     */
    public void vie(String calText) {
        MyRequest request = new MyRequest();
        request.setUri("api/game/vie");
        request.getData().put("calText", calText);
        sendRequest(request);
    }

    /**
     *  同步
     *  api/game/syn
     */
    public void syn() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/syn");
        sendRequest(request);
    }

    /**
     *  放弃
     *  api/game/giveUp
     */
    public void giveUp() {
        MyRequest request = new MyRequest();
        request.setUri("api/game/giveUp");
        sendRequest(request);
    }

    /**
     *  换牌
     */
    public synchronized void exchange(int resType, int resIndex, int tarType, int tarIndex) {
        MyRequest request = new MyRequest();
        request.setUri("api/game/exchange");
        request.getData().put("resType", resType);
        request.getData().put("resIndex", resIndex);
        request.getData().put("tarType", tarType);
        request.getData().put("tarIndex", tarIndex);
        sendRequest(request);
    }

}
