package clientSrc;

import clientSrc.paint.Game;

/**
 * @author dingsy
 * @date 2022/12/8
 * @apiNote
 *
 */
public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        game.runGame();
    }

}
