package clientSrc.effect;

import java.awt.*;

public class FlyCardSpecialEffect implements SpecialEffect{

    // 飞行过程中花得时间 越大越慢
    private long totalT = 700;
    private long t0;
    private Image image;
    private double resX1, resX2, resY1, resY2;
    private double tarX1, tarX2, tarY1, tarY2;

    public FlyCardSpecialEffect(Image image, double resX1, double resX2, double resY1, double resY2, double tarX1, double tarX2, double tarY1, double tarY2) {
        this.image = image;
        this.resX1 = resX1;
        this.resX2 = resX2;
        this.resY1 = resY1;
        this.resY2 = resY2;
        this.tarX1 = tarX1;
        this.tarX2 = tarX2;
        this.tarY1 = tarY1;
        this.tarY2 = tarY2;
        t0 = System.currentTimeMillis();
    }

    boolean over = false;

    @Override
    public void paint(Graphics2D g2d) {
        long curr = System.currentTimeMillis();
        if(curr - t0 >= totalT) {
            over = true;
            return;
        }
        double cent = ((double) curr - t0) / totalT;
        if(cent >= 0.01 && cent <= 0.99) {
            double x1 = (tarX1 - resX1) * cent + resX1;
            double x2 = (tarX2 - resX2) * cent + resX2;
            double y1 = (tarY1 - resY1) * cent + resY1;
            double y2 = (tarY2 - resY2) * cent + resY2;
            g2d.drawImage(image, (int) x1, (int) y1, (int) (x2-x1), (int) (y2-y1), null);
        }
    }

    @Override
    public boolean isOver() {
        return over;
    }

}
