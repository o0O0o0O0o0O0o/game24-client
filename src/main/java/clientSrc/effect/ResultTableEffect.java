package clientSrc.effect;

import javax.swing.*;
import java.awt.*;

/**
 * @author dingsy
 * @date 2023/1/6
 * @apiNote
 */
public class ResultTableEffect implements SpecialEffect{

    private boolean flag = true;
    private int openCloseTime = 1000;
    private int holdTime = 4000;
    private int step = 0;
    long time;
    private int tableWidth;
    private int tableHeight;
    private JTable jTable;

    public ResultTableEffect(int tableWidth, int tableHeight, JTable jTable) {
        this.tableWidth = tableWidth;
        this.tableHeight = tableHeight;
        this.jTable = jTable;
    }

    @Override
    public void paint(Graphics2D g2d) {
        long curr = System.currentTimeMillis();
        if(step == 0) {
            step = 1;
            time = System.currentTimeMillis();
            jTable.setSize(0, 0);
        }else if(step == 1) {
            if(curr - time <= openCloseTime) {
                double percent = (curr - time) / (double) openCloseTime;
                jTable.setSize((int) (tableWidth), (int) (tableHeight * percent));
                jTable.setVisible(true);
            }else {
                step = 2;
                time = System.currentTimeMillis();
            }
        }else if(step == 2) {
            if(curr - time > holdTime) {
                step = 3;
                time = System.currentTimeMillis();
            }
        }else if(step == 3) {
            if(curr - time <= openCloseTime) {
                double percent = 1 - ((curr - time) / (double) openCloseTime);
                jTable.setSize((int) (tableWidth), (int) (tableHeight * percent));
            }else {
                jTable.setVisible(false);
                flag = true;
            }
        }
    }

    @Override
    public boolean isOver() {
        return false;
    }
}
