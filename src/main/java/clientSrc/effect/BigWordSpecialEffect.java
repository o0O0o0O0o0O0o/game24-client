package clientSrc.effect;

import clientSrc.utils.G2dConfig;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

public class BigWordSpecialEffect implements SpecialEffect{

    private long step1 = 400;
    @Setter
    private long step2 = 4000;
    private long t0;
    private String word;
    private int x1, y1;
    private static JLabel label;
    private JComponent fatherComponent;

    public BigWordSpecialEffect(String word, int x1, int y1, JComponent fatherComponent) {
        this.word = word;
        this.x1 = x1;
        this.y1 = y1;
        this.fatherComponent = fatherComponent;
        t0 = System.currentTimeMillis();
    }

    boolean over = false;

    @Override
    public void paint(Graphics2D g2d) {
        long curr = System.currentTimeMillis();
        if(curr - t0 <= step2) {
            if(label == null) {
                label = new JLabel(word, SwingConstants.CENTER);
                label.setBackground(Color.gray);
                label.setOpaque(true);
                fatherComponent.add(label, 0);
            }
            int width = word.length()*20 + 10;
            label.setText(word);
            label.setSize(width, 30);
            label.setLocation(x1 - (width/2), y1);
            label.setFont(G2dConfig.big_word_font);
            label.setVisible(true);
            label.repaint();
        }else {
            label.setVisible(false);
            over = true;
            return;
        }
    }

    /*@Override
    public void paint(Graphics2D g2d) {
        long curr = System.currentTimeMillis();
        if(curr - t0 <= step2) {
            g2d.setColor(G2dConfig.gray);
            g2d.fillRect(x1-8, y1-20, word.length()*20 + 10, 30);
            g2d.setFont(G2dConfig.big_word_font);
            g2d.setColor(G2dConfig.black);
            g2d.drawString(word, x1, y1);
        }else {
            over = true;
            return;
        }
    }*/

    @Override
    public boolean isOver() {
        return over;
    }

}
