package clientSrc.effect;

import lombok.Getter;

import java.awt.*;

public class BigSignSpecialEffect implements SpecialEffect{


    private long step1 = 400;
    @Getter
    private long step2 = 4000;
    private long t0;
    private Image image;
    private int xo, yo;
    private int width = 220;
    private int height = 220;

    public BigSignSpecialEffect(Image image, int xo, int yo) {
        this.image = image;
        this.xo = xo;
        this.yo = yo;
        t0 = System.currentTimeMillis();
    }

    boolean over = false;

    @Override
    public void paint(Graphics2D g2d) {
        long curr = System.currentTimeMillis();
        if(curr - t0 <= step1) {
            double percent = ((double)(curr - t0))/step1;
            g2d.drawImage(image,
                    (int) (xo - percent*width/2),
                    (int) (yo - percent*height/2),
                    (int) (width*percent),
                    (int) (height*percent),
                    null);
        }else if(curr - t0 <= step2) {
            g2d.drawImage(image, xo - width/2, yo - height/2, width, height, null);
        }else {
            over = true;
            return;
        }
    }

    @Override
    public boolean isOver() {
        return over;
    }

}
