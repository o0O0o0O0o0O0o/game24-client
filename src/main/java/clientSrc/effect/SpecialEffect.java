package clientSrc.effect;

import java.awt.*;

public interface SpecialEffect {

    void paint(Graphics2D g2d);

    boolean isOver();
}
