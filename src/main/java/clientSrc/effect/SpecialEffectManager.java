package clientSrc.effect;

import clientSrc.utils.RefreshUtil;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SpecialEffectManager {

    private static int refreshTime = 20;
    private List<SpecialEffect> effectList = new ArrayList<>();

    public void addEffect(SpecialEffect effect) {
        effectList.add(effect);
        RefreshUtil.refreshTime = refreshTime;
    }

    public void paint(Graphics2D g2d) {
        if(effectList.size() == 0) {
            RefreshUtil.refreshTime = RefreshUtil.defaultRefreshTime;
            return;
        }
        List<SpecialEffect> lost = new ArrayList<>();
        for(SpecialEffect effect: effectList) {
            if(effect.isOver()) {
                lost.add(effect);
                continue;
            }else {
                effect.paint(g2d);
            }
        }
        effectList.removeAll(lost);
    }

}
