package clientSrc.utils;

import clientSrc.entity.MyApi;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dingsy
 * @date 2022/12/16
 * @apiNote
 */
public class G2dConfig {

    public static final Color white     = new Color(255, 255, 255);
    public static final Color lightGray = new Color(192, 192, 192);
    public static final Color gray      = new Color(128, 128, 128);
    public static final Color darkGray  = new Color(64, 64, 64);
    public static final Color black     = new Color(0, 0, 0);
    public static final Color red       = new Color(255, 0, 0);
    public static final Color pink      = new Color(255, 175, 175);
    public static final Color orange    = new Color(255, 200, 0);
    public static final Color yellow    = new Color(255, 255, 0);
    public static final Color yellow2    = new Color(210, 164, 23);
    public static final Color green     = new Color(0, 255, 0);
    public static final Color magenta   = new Color(255, 0, 255);
    public static final Color cyan      = new Color(0, 255, 255);
    public static final Color blue      = new Color(0, 0, 255);
    public static final Color dark_orange = new Color(70, 60, 0);

    public static BasicStroke 粗虚线 = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0, new float[]{8f, 4f}, 0);
    public static BasicStroke 粗实线 = new BasicStroke(2);
    public static BasicStroke 对手分隔线 = new BasicStroke(2);
    public static BasicStroke 普通细线 = new BasicStroke(1);
    public static Font name_score_font = new Font("微软雅黑", Font.BOLD, 16);
    public static Font text_field_font = new Font("微软雅黑", Font.BOLD, 14);
    public static Font big_word_font = new Font("微软雅黑", Font.BOLD, 18);
    public static Font table_title_font = new Font("微软雅黑", Font.BOLD, 14);
    public static Font table_data_font = new Font("微软雅黑", Font.PLAIN, 14);

    @MyApi("vie.png")
    public static Image vieImg;
    @MyApi("631f79f5be4373309cb82147b87f7245.jpeg")
    public static Image cardBackImg;
    @MyApi("blank.jpeg")
    public static Image blankImg;
    @MyApi("plusBtn.jpeg")
    public static Image plusBtnImg;
    @MyApi("minusBtn.jpeg")
    public static Image minusBtnImg;
    @MyApi("default.jpeg")
    public static Image defaultImg;
    @MyApi("0.jpeg")
    public static Image cardImage_0;


    private static Map<String, Image> bufferMap = new HashMap<>();
    // 传入什么 就返回什么图片
    public static Image getCardImg(String name) {
        Image image = bufferMap.get(name);
        if(image == null) {
            return defaultImg;
        }
        return image;
    }

    public static void updateImg(java.util.List<String> nameList) {
        for(String name: nameList) {
            try {
                if(bufferMap.containsKey(name)) continue;
                Image image = ImageIO.read(G2dConfig.class.getResource("/pic/"+name+".jpeg"));
                bufferMap.put(name, image);
            } catch (IOException|IllegalArgumentException e) {
                System.out.println("找不到文件：" + name + ".jpeg");
            }
        }
    }


    static {
        Class clazz = G2dConfig.class;
        Field[] fields = clazz.getFields();
        for(Field field: fields) {
            MyApi annotation = field.getAnnotation(MyApi.class);
            if(annotation != null) {
                String path = annotation.value();
                // 获取图片
                BufferedImage image = null;
                try {
                    image = ImageIO.read(G2dConfig.class.getResource("/pic/"+path));
                } catch (IOException|IllegalArgumentException e) {
                    System.out.println("找不到文件：" + path);
                    path = "default.jpeg";
                    try {
                        image = ImageIO.read(G2dConfig.class.getResource("/pic/"+path));
                    } catch (IOException|IllegalArgumentException ex) {
                        ex.printStackTrace();
                    }
                }
                try {
                    field.set(null, image);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                bufferMap.put(field.getName(), image);
            }
        }
    }


}
