package clientSrc;

import clientSrc.entity.Card;
import clientSrc.entity.MyApi;
import clientSrc.entity.MyRequest;
import clientSrc.entity.SynAllDataVo;
import clientSrc.paint.Game;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dingsy
 * @date 2022/12/15
 * @apiNote
 */
public class RequestAccepter {

    private Game game;
    private Map<String, Method> methodMap = new HashMap<>();

    public RequestAccepter(Game game) {
        this.game = game;
    }

    {
        Class clazz = this.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for(Method method: methods) {
            MyApi annotation = method.getAnnotation(MyApi.class);
            if(annotation != null) {
                methodMap.put(annotation.value(), method);
            }
        }
    }

    public void processRequestLine(String line) {
        MyRequest request = JSON.parseObject(line, MyRequest.class);
        System.out.println("process ----> " + line);
        if(!methodMap.containsKey(request.getUri())) {
//            System.out.println("uri不存在：" + request.getUri());
        }
        Method method = methodMap.get(request.getUri());
        Parameter[] parameters = method.getParameters();
        Object[] invokeParams = new Object[parameters.length];
        invokeParams[0] = request.getData();

        try {
            method.invoke(this, invokeParams);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    // 通知客户端把牌堆的排发到用户手牌区域
    @MyApi("game/dealToHand")
    void dealToHand(Map<String, Object> data){
        game.dealToHand((String) data.get("playerId"), (Integer) data.get("handIndex"));
    }

    // 通知客户端把牌堆的排发到抢牌区
    @MyApi("game/dealToVieCard")
    void dealToVieCard(Map<String, Object> data) {
        JSONObject cardObj = (JSONObject) data.get("card");
        Card card = cardObj.toJavaObject(Card.class);
        game.dealToVieCard(card);
    }

    // 通知客户端有用户抢牌
    @MyApi("game/vieCard")
    void vieCard(Map<String, Object> data) {
        game.vieCard((String) data.get("playerId"), (String) data.get("text"));
    }

    // 向客户端同步数据
    @MyApi("game/syn")
    void syn(Map<String, Object> data) {
        String dataVoJson = data.get("dataVo").toString();
        SynAllDataVo dataVo = JSON.parseObject(dataVoJson, SynAllDataVo.class);
        game.syn(dataVo);
    }

    // 通知客户端展示大字
    @MyApi("game/bigTitle")
    void showAllBigTitle(Map<String, Object> data) {
        game.showAllBigTitle((String) data.get("title"));
    }

    // 通知客户端展示提示框
    @MyApi("game/tip")
    void showTip(Map<String, Object> data) {
        game.showTip((String) data.get("text"));
    }

    // 展示结果
    @MyApi("game/showResult")
    void showResult(Map<String, Object> data) {
        String dataVoJson = data.get("result").toString();
        Object[][] result = JSON.parseObject(dataVoJson, Object[][].class);
        game.showResult(result);
    }

    // 乱七八糟
    @MyApi("game/hey")
    void hey(Map<String, Object> data) {
        String text = data.get("text").toString();
        game.updateImg(text);
    }

}
